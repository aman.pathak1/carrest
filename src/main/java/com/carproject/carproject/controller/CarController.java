package com.carproject.carproject.controller;

import com.carproject.carproject.exception.CarAlreadyExistsException;
import com.carproject.carproject.exception.CarNotFoundException;
import com.carproject.carproject.model.Car;
import com.carproject.carproject.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/")
public class CarController {
    @Autowired
    CarService carService;

    ResponseEntity<?> responseEntity;

    @PostMapping("cars")
public ResponseEntity<?> saveCar(@RequestBody Car car) throws CarAlreadyExistsException {
        try {
            responseEntity = new ResponseEntity<>(this.carService.saveCar(car), HttpStatus.CREATED);
        } catch(CarAlreadyExistsException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;

    }

    @GetMapping("cars/{id}")
    public ResponseEntity<?> getCarById(@PathVariable int id) throws CarNotFoundException {
        try {
            responseEntity = new ResponseEntity<>(this.carService.getCarById(id), HttpStatus.OK);
        } catch(CarNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("cars")
    public ResponseEntity<?> getAllCars(){
        try {
            responseEntity = new ResponseEntity<>(this.carService.getCarList(), HttpStatus.OK);
        }
        catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
    @GetMapping("cars/brand/{brand}")
    public ResponseEntity<?> getCarListByBrand(@PathVariable String brand){
        try {
            responseEntity = new ResponseEntity<>(this.carService.getCarListByBrand(brand), HttpStatus.OK);
        }
        catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("cars/rating/{rating}")
    public ResponseEntity<?> getCarByRatingGreaterThenInput(@PathVariable Double rating){
        try {
            responseEntity = new ResponseEntity<>(this.carService.getCarByRatingGreaterThenInput(rating), HttpStatus.OK);
        }
        catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("cars/price")
    public ResponseEntity<?> getCarInPriceRange(@RequestParam("lowestPrice") int lowerBound,
                                                @RequestParam("highestPrice") int higherBound){
        try {
            responseEntity = new ResponseEntity<>(this.carService.getCarInPriceRange(lowerBound,higherBound), HttpStatus.OK);
        }
        catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }


    @PutMapping("cars")
    public ResponseEntity<?> updateCar(@RequestBody Car car) throws CarNotFoundException{
        try {
            responseEntity = new ResponseEntity<>(this.carService.updateCar(car),HttpStatus.OK);
        } catch(CarNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @DeleteMapping("cars/{id}")
    public ResponseEntity<?> deleteCarById(@PathVariable int id) throws CarNotFoundException {
        try {
            responseEntity = new ResponseEntity<>(this.carService.deleteCarById(id), HttpStatus.OK);
        } catch(CarNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }


}
