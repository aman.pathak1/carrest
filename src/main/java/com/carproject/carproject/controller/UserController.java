package com.carproject.carproject.controller;


import com.carproject.carproject.exception.CarNotFoundException;
import com.carproject.carproject.exception.UserAlreadyExistsException;
import com.carproject.carproject.exception.UserNotFoundException;
import com.carproject.carproject.model.User;
import com.carproject.carproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/v1/")
public class UserController {
    @Autowired
    UserService userService;

    ResponseEntity<?> responseEntity;

    @PostMapping("users")
    public ResponseEntity<?> saveUser(@RequestBody User user) throws UserAlreadyExistsException {
        try {
            responseEntity = new ResponseEntity<>(this.userService.saveUser(user), HttpStatus.CREATED);
        } catch(UserAlreadyExistsException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;

    }

    @GetMapping("users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable int id) throws UserNotFoundException {
        try {
            responseEntity = new ResponseEntity<>(this.userService.getUserById(id), HttpStatus.OK);
        } catch(UserNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("users")
    public ResponseEntity<?> getAllUsers(){
        try {
            responseEntity = new ResponseEntity<>(this.userService.getUserList(), HttpStatus.OK);
        }
        catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
    @PutMapping("users")
    public ResponseEntity<?> updateUser(@RequestBody User user) throws UserNotFoundException{
        try {
            responseEntity = new ResponseEntity<>(this.userService.updateUser(user),HttpStatus.OK);
        } catch(UserNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable int id) throws UserNotFoundException {
        try {
            responseEntity = new ResponseEntity<>(this.userService.deleteUserById(id), HttpStatus.OK);
        } catch(UserNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }



}
