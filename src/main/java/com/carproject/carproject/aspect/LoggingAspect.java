package com.carproject.carproject.aspect;

import com.carproject.carproject.model.Car;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggingAspect {
    Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    HttpServletRequest httpServletRequest;

    @Before(value = "execution(* com.carproject.carproject.controller.*.*(..))")
    public void beforeAdvice(JoinPoint joinPoint){
        logger.info("Before Advice called for the method : " + joinPoint.getSignature());
        logger.info("Requested End Point : " + httpServletRequest.getRequestURL());
    }

    /*@AfterReturning(value = "execution(* com.carproject.carproject.service.CarService.saveCar(...))")
    public void afterReturningSaveCar(JoinPoint joinPoint, Car car){
        logger.info("After advice apllied for method:" +joinPoint.getSignature());
        logger.info("Car:" +car);
    }*/

        @Around(value = "execution(* com.carproject.carproject.service.*.*(..))")
        public Object aroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
            StopWatch stopWatch=new StopWatch();
            stopWatch.start();
            Object result= proceedingJoinPoint.proceed();
            stopWatch.stop();
            logger.info("Time taken for complete execution:{}",stopWatch.getTotalTimeMillis());
            return result;
        }

}
