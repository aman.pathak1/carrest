package com.carproject.carproject.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.security.auth.message.AuthException;
import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class SecurityAspect {
    Logger logger= LoggerFactory.getLogger(SecurityAspect.class);

    @Autowired
    HttpServletRequest httpServletRequest;

    @Before(value = "execution(* com.carproject.carproject.controller.*.*(..))")
    public void checkSecurity(JoinPoint joinPoint) throws AuthException {
        String authorization = httpServletRequest.getHeader("Authorization");
        if(authorization == null)
            throw new AuthException("No Authorization Header");

        String bearerToken = authorization.split(" ")[1];
        String bearerTokenValue = "challahasdaphire";
        if(!bearerToken.equals(bearerTokenValue))
            throw new AuthException("Invalid Bearer Token");
    }



}
