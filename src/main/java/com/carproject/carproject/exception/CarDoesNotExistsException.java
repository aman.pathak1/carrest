package com.carproject.carproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT , reason = "car does not exists with given id")
public class CarDoesNotExistsException extends Exception {

}
