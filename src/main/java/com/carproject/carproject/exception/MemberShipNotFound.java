package com.carproject.carproject.exception;

public class MemberShipNotFound extends Exception {
    public MemberShipNotFound(String message) {
        super(message);
    }
}
