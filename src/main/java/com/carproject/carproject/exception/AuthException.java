package com.carproject.carproject.exception;

public class AuthException extends Exception {
    public AuthException(String message) {
        super(message);
    }
}
