package com.carproject.carproject.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT , reason = "car not found for this brand")
public class BrandNotFoundException extends Exception {
}
