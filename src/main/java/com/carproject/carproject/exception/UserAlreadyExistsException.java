package com.carproject.carproject.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT , reason = "user already exists with this id")
public class UserAlreadyExistsException extends Exception {
}
