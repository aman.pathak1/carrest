package com.carproject.carproject.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT , reason = "user does not exists with this id")
public class UserNotFoundException extends Exception {
}
