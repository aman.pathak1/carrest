package com.carproject.carproject.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT , reason = "car already exists with given id")
public class CarAlreadyExistsException extends Exception {

}
