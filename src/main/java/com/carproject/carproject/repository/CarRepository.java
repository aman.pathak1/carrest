package com.carproject.carproject.repository;

import com.carproject.carproject.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends JpaRepository<Car,Integer> {

    Optional<Car> findByName(String name);

    List<Car> findByBrand(String brand);
}
