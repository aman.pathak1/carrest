package com.carproject.carproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
public class User {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column
    private int id;
    @Column
   private String name;
    @Column
   private  String email;
    @Column
   private String  contactNo;
    @Column
    private String membership;

}
