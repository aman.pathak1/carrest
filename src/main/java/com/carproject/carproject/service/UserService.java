package com.carproject.carproject.service;

import com.carproject.carproject.exception.CarAlreadyExistsException;
import com.carproject.carproject.exception.CarNotFoundException;
import com.carproject.carproject.exception.UserAlreadyExistsException;
import com.carproject.carproject.exception.UserNotFoundException;
import com.carproject.carproject.model.Car;
import com.carproject.carproject.model.User;
import com.carproject.carproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService{

    @Autowired
    UserRepository userRepository;

    @Override
    public User saveUser(User user) throws UserAlreadyExistsException {
        Optional<User> userById = this.userRepository.findByName(user.getName());
        if(userById.isPresent())
            throw new UserAlreadyExistsException();
        else
            return this.userRepository.save(user);

    }

    @Override
    public User getUserById(int id) throws UserNotFoundException {
        User user = null;
        Optional<User> userById = this.userRepository.findById(id);

        if(!userById.isPresent())
            throw new UserNotFoundException();
        else
            user = userById.get();

        return user;
    }

    @Override
    public List<User> getUserList() {
        return this.userRepository.findAll();
    }

    @Override
    public User updateUser(User user) throws UserNotFoundException {
        Optional<User> userById = this.userRepository.findById(user.getId());
        if(!userById.isPresent())
            throw new UserNotFoundException();
        else
            return this.userRepository.save(user);
    }

    @Override
    public Boolean deleteUserById(int id) throws UserNotFoundException {
        Optional<User> userById = this.userRepository.findById(id);
        if(!userById.isPresent())
            throw new UserNotFoundException();
        else
            this.userRepository.delete(userById.get());
          return true;
    }

}
