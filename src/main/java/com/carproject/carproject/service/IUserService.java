package com.carproject.carproject.service;


import com.carproject.carproject.exception.UserAlreadyExistsException;
import com.carproject.carproject.exception.UserNotFoundException;
import com.carproject.carproject.model.User;
import java.util.List;
public interface IUserService {
    User saveUser(User user) throws UserAlreadyExistsException;

    User getUserById(int id) throws UserNotFoundException;

    List<User> getUserList();

    User updateUser(User user) throws UserNotFoundException;

    Boolean deleteUserById(int id) throws UserNotFoundException;
//    Double discountAccordingToUserMemberShip(String membership, String carName) throws MemberShipNotFound, MemberShipNotFound;

}