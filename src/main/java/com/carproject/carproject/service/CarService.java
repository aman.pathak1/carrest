package com.carproject.carproject.service;

import com.carproject.carproject.exception.BrandNotFoundException;
import com.carproject.carproject.exception.CarAlreadyExistsException;
import com.carproject.carproject.exception.CarNotFoundException;
import com.carproject.carproject.model.Car;
import com.carproject.carproject.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarService implements ICarService {

    @Autowired
    CarRepository carRepository;

    @Override
    public Car saveCar(Car car) throws CarAlreadyExistsException {
        Optional<Car> carById = this.carRepository.findByName(car.getName());
        if(carById.isPresent())
            throw new CarAlreadyExistsException();
        else
            return this.carRepository.save(car);

    }

    @Override
    public Car getCarById(int id) throws CarNotFoundException {
        Car car = null;
        Optional<Car> carById = this.carRepository.findById(id);

        if(!carById.isPresent())
            throw new CarNotFoundException();
        else
            car = carById.get();

        return car;
    }

    @Override
    public List<Car> getCarList() {
        return this.carRepository.findAll();
    }

    @Override
    public Car updateCar(Car car) throws CarNotFoundException{

        Optional<Car> carById = this.carRepository.findById(car.getId());
        if(!carById.isPresent())
            throw new CarNotFoundException();
        else
            return this.carRepository.save(car);

    }

    @Override
    public List<Car> getCarListByBrand(String brand) throws BrandNotFoundException {


        List<Car> carListForBrandSpecific = carRepository.findAll().stream()
                .filter(car -> car.getBrand().equalsIgnoreCase(brand))
                .collect(Collectors.toList());
//        System.out.println(carListForBrandSpecific);
//        List<Car> carListForBrandSpecific = carRepository.findByBrand(brand);
        if(carListForBrandSpecific.isEmpty())
            throw new BrandNotFoundException();
        else
            return carListForBrandSpecific;

    }

    @Override
    public List<Car> getCarInPriceRange(int lowestPrice, int highestPrice) throws CarNotFoundException {

        List<Car> carListInPriceRange = carRepository.findAll().stream()
                .filter(car -> ((car.getPrice() >= lowestPrice) && (car.getPrice() <= highestPrice)))
                .collect(Collectors.toList());
        if(carListInPriceRange.isEmpty())
            throw new CarNotFoundException();
        else
            return  carListInPriceRange;
    }

    @Override
    public List<Car> getCarByRatingGreaterThenInput(Double rating) throws CarNotFoundException {
        List<Car> carListWithRatingGreaterThenInput = carRepository.findAll().stream()
                .filter(car -> car.getRating() > rating)
                .collect(Collectors.toList());
        if(carListWithRatingGreaterThenInput.isEmpty())
            throw new CarNotFoundException();
        else
            return carListWithRatingGreaterThenInput;

    }

    @Override
    public Boolean deleteCarById(int id) throws CarNotFoundException {
        Optional<Car> carById = this.carRepository.findById(id);
        if(!carById.isPresent())
            throw new CarNotFoundException();
        else
            this.carRepository.delete(carById.get());
            return true;
    }
}
