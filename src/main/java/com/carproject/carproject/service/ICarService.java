package com.carproject.carproject.service;


import com.carproject.carproject.exception.BrandNotFoundException;
import com.carproject.carproject.exception.CarAlreadyExistsException;
import com.carproject.carproject.exception.CarNotFoundException;
import com.carproject.carproject.model.Car;

import java.util.List;

public interface ICarService {

    Car saveCar(Car car) throws CarAlreadyExistsException;

    Car getCarById(int id) throws CarNotFoundException;

    List<Car> getCarList();

    Car updateCar(Car car) throws CarNotFoundException;

    Boolean deleteCarById(int id) throws CarNotFoundException;

    public List<Car> getCarListByBrand(String brand) throws BrandNotFoundException;
    public List<Car> getCarInPriceRange(int lowestPrice, int highestPrice) throws CarNotFoundException;

    List<Car> getCarByRatingGreaterThenInput(Double rating) throws CarNotFoundException;


}
